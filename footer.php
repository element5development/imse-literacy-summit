<?php 
/*----------------------------------------------------------------*\

	HTML FOOTER CONTENT
	Commonly only used to close off open containers
	External resources are not referanced here, refer to the functions.php

\*----------------------------------------------------------------*/
?>

<?php wp_footer(); ?>


<?php if ( 'event' == get_post_type() ) : ?>
	<?php
		$time = get_field('start_time', get_the_ID());
		$date = get_field('event_date', get_the_ID(), false, false);
		$date = new DateTime($date);
		// echo $date->format('Y/m/d'); echo $time; 
	?>
	<script type="text/javascript"> // Get the Countdown Values
		jQuery('.countdown').countdown('<?php echo $date->format('Y/m/d'); ?> <?php echo $time; ?>', function(event) {
			jQuery('.days p span').html(event.strftime('%D'));
			jQuery('.hours p span').html(event.strftime('%H'));
			jQuery('.minutes p span').html(event.strftime('%M'));
			jQuery('.seconds p span').html(event.strftime('%S'));
		});
	</script>
<?php endif; ?>

</body>

</html>