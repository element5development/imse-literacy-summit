<?php 
/*----------------------------------------------------------------*\

	EVENT SINGLE POST TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation-lp'); ?>

<header class="post-head">
	<h1><?php the_field('event_name'); ?></h1>
	<div>
		<svg>
			<use xlink:href="#calendar" />
		</svg>
		<p class="subhead"><?php the_field('event_date'); ?></p>
	</div>
	<div>
		<svg>
			<use xlink:href="#map-pin" />
		</svg>
		<p class="subhead"><?php the_field('event_location'); ?></p>
	</div>
	<div class="countdown">
		<div class="counter">
			<div class="days">
				<p>
					<span class="lexia-light">##</span>
					Days
				</p>
			</div>
			<div class="hours">
				<p>
					<span class="lexia-light">##</span>
					Hours
				</p>
			</div>
			<div class="minutes">
				<p>
					<span class="lexia-light">##</span>
					Minutes
				</p>
			</div>
			<div class="seconds">
				<p>
					<span class="lexia-light">##</span>
					Seconds
				</p>
			</div>
		</div>
	</div>
	<div>
		<?php $headerlink = get_field('header_button'); ?>
		<?php if( $headerlink ): ?>
		<a class="button is-painted" href="<?php echo $headerlink['url']; ?>"><?php echo $headerlink['title']; ?></a>
		<?php endif; ?>
	</div>
	<?php if ( get_field('callout_title') && get_field('additional_content') ) : ?>
		<div class="callout">
			<a href="#more">
				<div>
					<svg>
						<use xlink:href="#star" />
					</svg>
					<h4><?php the_field('callout_title'); ?></h4>
					<p><?php the_field('callout_description'); ?></p>
				</div>
			</a>
		</div>
	<?php endif; ?>
</header>

<?php $headerbg = get_field('header_background'); ?>
<div class="background-img" <?php if( $headerbg ) :?>style="background-image: url(<?php echo $headerbg['sizes']['xlarge']; ?>);"<?php endif; ?>>
	<svg id="wave" viewBox="0 0 1497 68">
		<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
			<path d="M0.6171875,0.05078125 L0.6171875,15.8789062 C172.497396,-1.67317708 409.408854,5.26171875 711.351562,36.6835938 C1164.26563,83.8164062 1237.26953,74.5820312 1496.27344,29.8320313 C1496.57552,29.5065104 1496.57552,19.5794271 1496.27344,0.05078125 L0.6171875,0.05078125 Z" fill="#FFFFFF"></path>
		</g>
	</svg>
</div>

<main id="main-content">
	<article>
		<?php if( get_field('why_attend') ): ?>
			<section class="why-attend">
				<div>
					<h4>Why Attend</h4>
					<?php the_field('why_attend'); ?>
				</div>

				<?php $attendbgleft = get_field('why_attend_background_left'); ?>
				<?php if( $attendbgleft ): ?>
				<img src="<?php echo $attendbgleft['sizes']['xlarge']; ?>" alt="<?php echo $attendbgleft['alt']; ?>">
				<?php endif; ?>

				<?php $attendbgright = get_field('why_attend_background_left'); ?>
				<?php if( $attendbgright ): ?>
				<img src="<?php echo $attendbgright['sizes']['xlarge']; ?>" alt="<?php echo $attendbgright['alt']; ?>">
				<?php endif; ?>
			</section>
		<?php endif; ?>
		<?php if( get_field('speakers_title') ): ?>
			<section class="speakers">
				<div>
					<?php if( have_rows('speakers') ): ?>
					<?php while ( have_rows('speakers') ) : the_row(); ?>
						<?php $headshot = get_sub_field('headshot'); ?>
						<img class="lazyload blur-up" data-expand="200" data-sizes="auto" src="<?php echo $headshot['sizes']['placeholder']; ?>" data-src="<?php echo $headshot['sizes']['large']; ?>" data-srcset="<?php echo $headshot['sizes']['small']; ?> 300w, <?php echo $headshot['sizes']['medium']; ?> 700w, <?php echo $headshot['sizes']['large']; ?> 1000w, <?php echo $headshot['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $headshot['alt']; ?>">
					<?php endwhile; ?>
					<?php endif; ?>
					<h2><?php the_field('speakers_title'); ?></h2>
					<h4>Keynote Speakers</h4>
					<div>
						<div><?php the_field('speakers_content'); ?></div>
						<div><?php the_field('speakers_content_right'); ?></div>
					</div>
				</div>
			</section>
		<?php endif; ?>
		<?php if( get_field('panel_speakers_title') ): ?>
			<section class="panel">
				<div>
					<?php if( have_rows('panel_speakers') ): ?>
					<?php while ( have_rows('panel_speakers') ) : the_row(); ?>
						<?php $panelheadshot = get_sub_field('panel_headshot'); ?>
						<img class="lazyload blur-up" data-expand="200" data-sizes="auto" src="<?php echo $panelheadshot['sizes']['placeholder']; ?>" data-src="<?php echo $panelheadshot['sizes']['large']; ?>" data-srcset="<?php echo $panelheadshot['sizes']['small']; ?> 300w, <?php echo $panelheadshot['sizes']['medium']; ?> 700w, <?php echo $panelheadshot['sizes']['large']; ?> 1000w, <?php echo $panelheadshot['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $panelheadshot['alt']; ?>">
					<?php endwhile; ?>
					<?php endif; ?>
					<h2><?php the_field('panel_speakers_title'); ?></h2>
					<h4>Panel Moderator</h4>
					<div><?php the_field('panel_speakers_content'); ?></div>
				</div>
			</section>
		<?php endif; ?>
		<?php if( get_field('left_content') && get_field('right_image') ): ?>
			<section class="media-text has-image-on-right">
				<div>			
					<?php $rightimage = get_field('right_image'); ?>
					<?php if( $rightimage ): ?>
					<img class="lazyload blur-up" data-expand="200" data-sizes="auto" src="<?php echo $rightimage['sizes']['placeholder']; ?>" data-src="<?php echo $rightimage['sizes']['large']; ?>" data-srcset="<?php echo $rightimage['sizes']['small']; ?> 300w, <?php echo $rightimage['sizes']['medium']; ?> 700w, <?php echo $rightimage['sizes']['large']; ?> 1000w, <?php echo $rightimage['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $rightimage['alt']; ?>">
					<?php endif; ?>
				</div>
				<div class="content">
					<?php the_field('left_content'); ?>
				</div>
			</section>
		<?php endif; ?>
		<?php if( get_field('right_content') && get_field('left_image') ): ?>
			<section class="media-text">
				<div>
					<?php $leftimage = get_field('left_image'); ?>
					<?php if( $leftimage ): ?>
					<img class="lazyload blur-up" data-expand="200" data-sizes="auto" src="<?php echo $leftimage['sizes']['placeholder']; ?>" data-src="<?php echo $leftimage['sizes']['large']; ?>" data-srcset="<?php echo $leftimage['sizes']['small']; ?> 300w, <?php echo $leftimage['sizes']['medium']; ?> 700w, <?php echo $leftimage['sizes']['large']; ?> 1000w, <?php echo $leftimage['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $leftimage['alt']; ?>">
					<?php endif; ?>
				</div>
				<div class="content">
					<?php the_field('right_content'); ?>
				</div>
			</section>
		<?php endif; ?>
		<?php if( get_field('map') ): ?>
			<?php $map = get_field('map'); ?>
			<section class="map" <?php if( $map ) :?>style="background-image: url(<?php echo $map['sizes']['xlarge']; ?>);"<?php endif; ?>>
				<div>
					<?php if( get_field('left_map_card_title') ): ?>
						<div>
							<h4><?php the_field('left_map_card_title'); ?></h4>
							<?php the_field('left_map_card_content'); ?>
							<div title="Add to Calendar" class="button is-text-link addtocalendar">
								<?php 
									$startdate = get_field('event_date', false, false);
									$startdate = new DateTime($startdate);
								?>
								<var class="atc_event">
									<var class="atc_date_start"><?php echo $startdate->format('m/d/Y'); ?> <?php the_field('start_time'); ?></var>
									<var class="atc_date_end"><?php echo $startdate->format('m/d/Y'); ?> <?php the_field('end_time'); ?></var>
									<var class="atc_timezone">America/Detroit</var>
									<var class="atc_title"><?php the_title(); ?></var>
									<var class="atc_description"><?php the_field('event_description'); ?></var>
									<var class="atc_location"><?php the_field('event_location'); ?></var>
								</var>
							</div>
						</div>
					<?php endif; ?>
					<?php if( get_field('right_map_card_title') ): ?>
						<div>
							<h4><?php the_field('right_map_card_title'); ?></h4>
							<?php the_field('right_map_card_content'); ?>
						</div>
					<?php endif; ?>
				</div>
			</section>
		<?php endif; ?>
		<?php if( get_field('ticket_title') ): ?>
			<section id="ticket" class="ticket">
				<div class="ticket-card">
					<div class="title">
						<h2><?php the_field('ticket_title'); ?></h2>
						<p><?php the_field('ticket_subtitle'); ?></p>
						<?php if( have_rows('whats_included') ): ?>
							<h4>What's Included</h4>
							<ul class="includes">
								<?php while ( have_rows('whats_included') ) : the_row(); ?>
									<li><?php the_sub_field('item'); ?></li>
								<?php endwhile; ?>
							</ul>
						<?php endif; ?>
					</div>
					<div class="price">
						<h2><?php the_field('ticket_price'); ?></h2>
					</div>
					<?php if ( get_field('registration_form_id') ) : ?>
						<div class="registration">
							<?php echo do_shortcode('[gravityform id="'.get_field('registration_form_id').'" title="false" description="false"]'); ?>
						</div>
					<?php endif; ?>
				</div>
			</section>
		<?php endif; ?>
		<?php if( get_field('additional_content') ): ?>
			<section id="more" class="additional">
				<div>
					<?php the_field('additional_content'); ?>
				</div>
			</section>
		<?php endif; ?>
			<?php if( get_field('conclusion_background') ): ?>
			<?php $conclusionbg = get_field('conclusion_background'); ?>
			<div class="conclusion background-img" <?php if( $conclusionbg ) :?>style="background-image: url(<?php echo $conclusionbg['sizes']['xlarge']; ?>);"<?php endif; ?>>
				<svg id="wave" viewBox="0 0 1497 68">
					<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
						<path d="M0.6171875,0.05078125 L0.6171875,15.8789062 C172.497396,-1.67317708 409.408854,5.26171875 711.351562,36.6835938 C1164.26563,83.8164062 1237.26953,74.5820312 1496.27344,29.8320313 C1496.57552,29.5065104 1496.57552,19.5794271 1496.27344,0.05078125 L0.6171875,0.05078125 Z" fill="#FFFFFF"></path>
					</g>
				</svg>
				<div>
					<?php the_field('conclusion_content'); ?>
				</div>
			</div>
		<?php endif; ?>
	</article>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>