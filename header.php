<?php 
/*----------------------------------------------------------------*\

	HTML HEAD CONTENT
	Commonly contains site meta data and tracking scripts.
	External resources are not referanced here, refer to the functions.php

\*----------------------------------------------------------------*/
?>

<!doctype html>
<html xml:lang="en" lang="en">

<head>

	<?php //general stuff ?>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="stylesheet" href="https://use.typekit.net/wdq7igo.css">
	<meta name="google-site-verification" content="mjCVEpWeGSMxeuomUnH2U4mtrF1SVwNS1MHYrQdDQXo" />
	<meta name="msvalidate.01" content="D7FF62B50B5AC4465FB383958CD4A132" />
	<?php
	/*----------------------------------------------------------------*\
	|
	|	title, social and other seo tags are all handled and inserted 
	| via The SEO Framework which is a must have plugin
	|
	\*----------------------------------------------------------------*/
	?>
	<?php // force Internet Explorer to use the latest rendering engine available ?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<?php // mobile meta (hooray!) ?>
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<?php wp_head(); ?>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-PDCTDRV');</script>
	<!-- End Google Tag Manager -->
</head>

<body <?php body_class(); ?>>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PDCTDRV"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	<a id="skip-to-content" href="#main-content">Skip to main content</a>
	<?php get_template_part('template-parts/icon-set'); ?>
	<?php get_template_part('template-parts/elements/cookie'); ?>

	<div id="preload">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/brushstroke-hover.png" alt="Button Hover" />
	</div>