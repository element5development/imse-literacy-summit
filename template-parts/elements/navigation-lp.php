<?php 
/*----------------------------------------------------------------*\

	PRIMARY NAVIGATION

\*----------------------------------------------------------------*/
?>
<div class="primary-navigation">
	<nav>
		<a href="https://imse.com/">
			<svg>
				<use xlink:href="#logo" />
			</svg>
		</a>
	</nav>
</div>