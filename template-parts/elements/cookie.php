<?php 
/*----------------------------------------------------------------*\

	COOKIE USE NOTIFICATION BAR
	cookies used must be cleared via WPengine support

\*----------------------------------------------------------------*/
?>
<div class="cookie-useage-notification note-on">
	<div> 
		<button> 
			<svg><use xlink:href="#close"></use></svg> 
		</button>
		<p>This site uses cookies to provide you with a greater user experience. By using our website, you accept our <a target="_blank" href="https://imse.com/privacy-policy/">use of cookies</a>.</p>
	</div>
</div>