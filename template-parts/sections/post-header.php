<?php 
/*----------------------------------------------------------------*\

	POST HEADER
	Display the post title

\*----------------------------------------------------------------*/
?>

<header class="post-head">
	<h1><?php the_title(); ?></h1>
	<?php if ( get_field('subheader') ) : ?>
		<p><?php the_field('subheader'); ?></p>
	<?php endif; ?>
</header>