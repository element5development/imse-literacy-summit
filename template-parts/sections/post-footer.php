<?php 
/*----------------------------------------------------------------*\

	POST FOOTER
	Display copyright and navigation

\*----------------------------------------------------------------*/
?>

<footer class="post-footer">
	<div class="copyright">
		<div>
			<p>© <?php echo date('Y'); ?> <?php echo get_bloginfo( 'name' ); ?>. All Rights Reserved.</p>
			<?php wp_nav_menu(array( 'theme_location' => 'legal_navigation' )); ?>
		</div>
		<div id="element5-credit">
			<a target="_blank" href="https://element5digital.com" rel="nofollow">
				<img src="https://element5digital.com/wp-content/themes/e5-starting-point/dist/images/element5_credit_alt.svg" alt="Crafted by Element5 Digital" />
			</a>
		</div>
	</div>
</footer>