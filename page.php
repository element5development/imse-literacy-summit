<?php 
/*----------------------------------------------------------------*\

	DEFAULT PAGE TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/sections/post-header'); ?>

<main id="main-content">
	<?php if( have_rows('article') ):  ?>
		<article>
			<?php 
			/*----------------------------------------------------------------*\
			|
			| Insert page content which is most often handled via ACF Pro
			| and highly recommend the use of the flexiable content so
			|	we already placed that code here.
			|
			| https://www.advancedcustomfields.com/resources/flexible-content/
			|
			\*----------------------------------------------------------------*/
			?>
			<?php
				while ( have_rows('article') ) : the_row();
					if( get_row_layout() == 'editor' ):
						get_template_part('template-parts/sections/article/editor');
					elseif( get_row_layout() == '2editor' ):
						get_template_part('template-parts/sections/article/editor-2-column');
					elseif( get_row_layout() == '3editor' ):
						get_template_part('template-parts/sections/article/editor-3-column');
					elseif( get_row_layout() == 'media+text' ):
						get_template_part('template-parts/sections/article/media-text');
					elseif( get_row_layout() == 'cover' ):
						get_template_part('template-parts/sections/article/cover');
					elseif( get_row_layout() == 'gallery' ):
						get_template_part('template-parts/sections/article/gallery');
					endif;
				endwhile;
			?>
			<?php 
				$eventID = $_GET['eventid'];
				if ( $eventID ) : 
			?>
				<?php if( get_field('map', $eventID) ): ?>
					<?php $map = get_field('map', $eventID); ?>
					<section class="map" <?php if( $map ) :?>style="background-image: url(<?php echo $map['sizes']['xlarge']; ?>);"<?php endif; ?>>
						<div>
							<?php if( get_field('left_map_card_title', $eventID) ): ?>
								<div>
									<h4><?php the_field('left_map_card_title', $eventID); ?></h4>
									<?php the_field('left_map_card_content', $eventID); ?>
									<div title="Add to Calendar" class="button is-text-link addtocalendar">
										<?php 
											$startdate = get_field('event_date', $eventID, false, false);
											$startdate = new DateTime($startdate);
										?>
										<var class="atc_event">
											<var class="atc_date_start"><?php echo $startdate->format('m/d/Y'); ?> <?php the_field('start_time', $eventID); ?></var>
											<var class="atc_date_end"><?php echo $startdate->format('m/d/Y'); ?> <?php the_field('end_time', $eventID); ?></var>
											<var class="atc_timezone">America/Detroit</var>
											<var class="atc_title"><?php echo get_the_title($eventID); ?></var>
											<var class="atc_description"><?php the_field('event_description', $eventID); ?></var>
											<var class="atc_location"><?php the_field('event_location', $eventID); ?></var>
										</var>
									</div>
								</div>
							<?php endif; ?>
							<?php if( get_field('right_map_card_title', $eventID) ): ?>
								<div>
									<h4><?php the_field('right_map_card_title', $eventID); ?></h4>
									<?php the_field('right_map_card_content', $eventID); ?>
								</div>
							<?php endif; ?>
						</div>
					</section>
				<?php endif; ?>
			<?php endif; ?>
		</article>
	<?php else : ?>
		<article>
			<section class="is-narrow">
				<h2>Uh Oh. Something is missing.</h2>
				<p>Looks like this page has no content.</p>
			</section>
		</article>
	<?php endif; ?>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>